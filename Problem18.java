import java.util.Scanner;
public class Problem18 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int choice;
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        choice = sc.nextInt();
        if(choice==1){
            method1();
        }else if(choice==2){
            method2();
        }else if(choice==3){
            method3();
        }else if(choice==4){
            method4();
        }else if(choice==5){
            System.out.println("Bye bye!!!");
        }else{
            System.out.println("Error: Please input number between 1-5");
        }
        sc.close();
    }

    static void method1(){
        int num,i=0,j=0;
        Scanner sc=new Scanner(System.in);
        System.out.print("Please input number: ");
        num = sc.nextInt();
        while(i < num){
            while(j<=i){
                System.out.print("*");
                j++;
            }
            System.out.println();
            j=0;
            i++;
        }
        sc.close();
    }

    static void method2(){
        int num,i=0,j;
        Scanner sc=new Scanner(System.in);
        System.out.print("Please input number: ");
        num = sc.nextInt();
        while(i < num){
            j = num;
            while(j>i){
                System.out.print("*");
                j--;
            }
            System.out.println();
            i++;
        }
        sc.close();
    }

    static void method3(){
        int num,i=0,j=5,k=0;
        Scanner sc=new Scanner(System.in);
        System.out.print("Please input number: ");
        num = sc.nextInt();
        j = num;
        while(i<j){
            while(k<i){
                System.out.print(" ");
                k++;
            }
            while(j>i){
                System.out.print("*");
                j--;
            }
            System.out.println();
            k=0;
            j = num;
            i++;
        }
        sc.close();
    }
    static void method4(){
        int num,i=1,j=0,k;
        Scanner sc=new Scanner(System.in);
        System.out.print("Please input number: ");
        num = sc.nextInt();
        while(i<num+1){
            k=num;
            while(k>i){
                System.out.print(" ");
                k--;
            }
            while(j<i){
                System.out.print("*");
                j++;
                
            }
            System.out.println();
            j = 0;
            i++;
        }
        sc.close();
    }
}